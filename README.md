# dmenu - dynamic menu

My build of dmenu, the efficient dynamic menu for X.

### Patches
* Fuzzymatch
* Lineheight

### Color schemes
* Gruvbox
* Amora

Requirements
------------
* Xlib headers.
* ttf-iosevka-terminal

Installation
------------
Configure config.h to your liking and/or run:

    (sudo) make clean install
         
Running dmenu
-------------
`dmenu_run -flags`

* `-i` case insensitivity
* `-h` set height of bar

Or check the man page for more information.
         
